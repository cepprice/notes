package com.example.notes.activity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.notes.R;
import com.example.notes.custom.NoteAdapter;
import com.example.notes.database.NoteContract;
import com.example.notes.database.NoteDbHelper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class NotesListActivity extends AppCompatActivity {

    private static final String TAG = "NotesList";

    private Toolbar mToolbar;

    private RecyclerView mRecView;

    private NoteAdapter mAdapter;

    private NoteDbHelper dbHelper;
    private SQLiteDatabase mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_list);

        mToolbar = findViewById(R.id.toolbar_notes);
        setSupportActionBar(mToolbar);

        dbHelper = new NoteDbHelper(this);
        mDb = dbHelper.getWritableDatabase();

        mRecView = findViewById(R.id.rec_view);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                deleteNote((long) viewHolder.itemView.getTag());
            }
        }).attachToRecyclerView(mRecView);

    }


    @Override
    protected void onResume() {
        super.onResume();

        Cursor cursor = mDb.query(NoteContract.NoteEntry.TABLE_NAME,
                null, null, null, null, null,
                NoteContract.NoteEntry.COLUMN_TIMESTAMP + " DESC");

        mAdapter = new NoteAdapter(this, cursor);
        mRecView.setAdapter(mAdapter);
        mRecView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbHelper.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_add_note:
                addNote();
                return true;
//            case R.id.action_search:
//                searchNotes();
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void addNote() {
        Intent intent = new Intent(this, NewNoteActivity.class);
        startActivity(intent);
    }

    private void deleteNote(long id) {
        mDb.delete(NoteContract.NoteEntry.TABLE_NAME,
                NoteContract.NoteEntry.COLUMN_ID + " = " + id, null);
        mAdapter.swapCursor(getAllNotes());
    }

    private Cursor getAllNotes() {
        return mDb.query(
                NoteContract.NoteEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                NoteContract.NoteEntry.COLUMN_ID + " DESC"
        );
    }


    private void searchNotes() {
        //TODO
    }

}
