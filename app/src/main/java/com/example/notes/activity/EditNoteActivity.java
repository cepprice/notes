package com.example.notes.activity;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.example.notes.R;
import com.example.notes.database.NoteContract;
import com.example.notes.database.NoteDbHelper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class EditNoteActivity extends AppCompatActivity implements View.OnFocusChangeListener {

    private String TAG = "EditNote";

    private EditText mEdtTitle;
    private EditText mEdtBody;
    private Toolbar mToolbar;

    private boolean mNoteChanged = false;

    private SQLiteDatabase mDb;

    private TextWatcher mWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {}

        @Override
        public void afterTextChanged(Editable s) {
            mNoteChanged = true;
            mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_done));
            mToolbar.setNavigationOnClickListener(v -> {
                String title = mEdtTitle.getText().toString();
                if (title.replaceAll("\\s+", "").equals("")) {
                    Toast.makeText(EditNoteActivity.this,
                            "Title cannot be blank", Toast.LENGTH_LONG).show();
                    return;
                }

                mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
                hideKeyboard();
                removeFocus();
                mToolbar.setNavigationOnClickListener(v1 -> {
                    saveNote();
                    finish();
                });
            });
            Log.d(TAG, "note was changed");
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);

        mToolbar = findViewById(R.id.toolbar_edit_note);
        toolbarActions();

        mEdtTitle = findViewById(R.id.edt_note_title);
        mEdtBody = findViewById(R.id.edt_note_body);
        mEdtTitle.setText(getIntent().getStringExtra("TITLE"));
        mEdtBody.setText(getIntent().getStringExtra("BODY"));
        mEdtTitle.setOnFocusChangeListener(this);
        mEdtBody.setOnFocusChangeListener(this);
        mEdtTitle.addTextChangedListener(mWatcher);
        mEdtBody.addTextChangedListener(mWatcher);

        mDb = new NoteDbHelper(this).getWritableDatabase();
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        int id = v.getId();
        if (id == R.id.edt_note_title || id == R.id.edt_note_body) {
            if (hasFocus) {
                mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_done));
                mToolbar.setNavigationOnClickListener(v1 -> {
                    hideKeyboard();
                    removeFocus();
                    mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
                    mToolbar.setNavigationOnClickListener(v2 -> {
                        finish();
                    });
                });
            }
        }
    }



    @Override
    public void onBackPressed() {
        Log.d(TAG, "back pressed");
        if (mNoteChanged) {
            new AlertDialog.Builder(this)
                    .setTitle("Discard?")
                    .setMessage("Changes not saved. Are you sure want to exit?")
                    .setPositiveButton("ok", (d1, w1) -> {
                        finish();
                        d1.dismiss();
                    }).setNegativeButton("cancel", (d2, w2) -> {
                        d2.dismiss();
                    }).create().show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar_new_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_delete_note) {
            //TODO Create dialog
            deleteNote();
            finish();
        } else {
            return super.onOptionsItemSelected(item);
        }

        return true;
    }

    private void toolbarActions() {
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        mToolbar.setNavigationOnClickListener(v -> {
                    finish();
        });
    }


    private void saveNote() {
        String title = mEdtTitle.getText().toString();
        if (title.replaceAll("\\s+", "").equals("")) {
            Toast.makeText(this, "Title cannot be blank", Toast.LENGTH_LONG).show();
            return;
        }
        String body = mEdtBody.getText().toString();
        String id = String.valueOf(getIntent().getLongExtra("ID", 0));
        String time = getDateTime();

        ContentValues cv = new ContentValues();
        cv.put(NoteContract.NoteEntry.COLUMN_TITLE, title);
        cv.put(NoteContract.NoteEntry.COLUMN_BODY, body);
        cv.put(NoteContract.NoteEntry.COLUMN_TIMESTAMP, time);
        Log.d(TAG, time);

        mDb.update(NoteContract.NoteEntry.TABLE_NAME, cv,
                NoteContract.NoteEntry.COLUMN_ID + " = ?", new String[] {id});
        Toast.makeText(getApplicationContext(), "Saving note..", Toast.LENGTH_SHORT).show();

        Log.d(TAG, "existing note edited");
    }

    private void deleteNote() {
        String id = String.valueOf(getIntent().getLongExtra("ID", 0));

        mDb.delete(NoteContract.NoteEntry.TABLE_NAME,
                NoteContract.NoteEntry.COLUMN_ID + " = " + id, null);
    }

    private void removeFocus() {
        mEdtTitle.clearFocus();
        mEdtBody.clearFocus();
    }

    private void hideKeyboard() {
        Log.d(TAG, "hideKeyboard");
        Activity activity = EditNoteActivity.this;
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            Log.d(TAG, "empty view");
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat =
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }


}
