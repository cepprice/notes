package com.example.notes.activity;

import android.app.Activity;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.example.notes.R;
import com.example.notes.database.NoteContract;
import com.example.notes.database.NoteDbHelper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class NewNoteActivity extends AppCompatActivity implements View.OnFocusChangeListener, View.OnKeyListener {

    private static final String TAG = "NewNote";

    private EditText mEdtTitle;
    private EditText mEdtBody;

    private Toolbar mToolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note);

        mToolbar = findViewById(R.id.toolbar_new_note);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mEdtTitle = findViewById(R.id.edt_note_title);
        mEdtBody = findViewById(R.id.edt_note_body);

        toolbarActions();

        mEdtTitle.setOnKeyListener(this);
        mEdtBody.setOnKeyListener(this);

    }


    private void toolbarActions() {
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_done));
        mToolbar.setNavigationOnClickListener(v -> {
            hideKeyboard();
            removeFocus();

            String title = mEdtTitle.getText().toString();
            if (title.replaceAll("\\s+", "").equals("")) {
                Toast.makeText(this, "Title cannot be blank", Toast.LENGTH_LONG).show();
            }
            else {
                mEdtTitle.setOnFocusChangeListener(this);
                mEdtBody.setOnFocusChangeListener(this);

                mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
                mToolbar.setNavigationOnClickListener(v1 -> {
                    saveNote();
                    finish();
                });
            }
        });
    }

    private void removeFocus() {
        Log.d(TAG, "removeFocus()");
        mEdtTitle.clearFocus();
        mEdtBody.clearFocus();
    }

    private void hideKeyboard() {
        Log.d(TAG, "hideKeyboard");
        Activity activity = NewNoteActivity.this;
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    @Override
    public boolean onSupportNavigateUp() {
        hideKeyboard();
        removeFocus();
        Log.d(TAG, "onSupportNavigateUp");
        //TODO: Say that not won't be saved
        return true;
    }

    private void saveNote() {
        String title = mEdtTitle.getText().toString();
        String body = mEdtBody.getText().toString();
        String time = getDateTime();

        ContentValues cv = new ContentValues();
        cv.put(NoteContract.NoteEntry.COLUMN_TITLE, title);
        cv.put(NoteContract.NoteEntry.COLUMN_BODY, body);
        cv.put(NoteContract.NoteEntry.COLUMN_TIMESTAMP, time);

        Log.d(TAG, time);

        NoteDbHelper dbHelper = new NoteDbHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.insert(NoteContract.NoteEntry.TABLE_NAME, null, cv);
        db.close();
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            Log.d(TAG, "ToolbarActions() called after focus change");
            toolbarActions();
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        int id = v.getId();
        if (id == R.id.edt_note_body || id ==R.id.edt_note_title) {
            if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                hideKeyboard();
                removeFocus();
                return true;
            }
        }
        return false;
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat =
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }
}
