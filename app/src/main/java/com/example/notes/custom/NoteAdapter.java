package com.example.notes.custom;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.notes.R;
import com.example.notes.activity.EditNoteActivity;
import com.example.notes.database.NoteContract;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteViewHolder> {

    private String TAG = "adapter";

    private Context mContext;
    private Cursor mCursor;

    public NoteAdapter(Context context, Cursor cursor) {
        mContext = context;
        mCursor = cursor;
    }

    public class NoteViewHolder extends RecyclerView.ViewHolder  {

        private TextView txtNoteTitle;
        private TextView txtNoteBody;

        public NoteViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNoteTitle = itemView.findViewById(R.id.list_item_title);
            txtNoteBody = itemView.findViewById(R.id.list_item_body);

            itemView.setOnClickListener(v -> {
                Context ctx = v.getContext();
                Intent intent = new Intent(ctx, EditNoteActivity.class);
                intent.putExtra("ID", (long) itemView.getTag());
                intent.putExtra("TITLE", txtNoteTitle.getText());
                intent.putExtra("BODY", txtNoteBody.getText());
                ctx.startActivity(intent);
            });
        }


    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater
                .from(mContext)
                .inflate(R.layout.list_item, parent, false);
        return new NoteViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteViewHolder holder, int position) {
        if (!mCursor.moveToPosition(position)) {
            return;
        }

        String title = mCursor.getString(mCursor.getColumnIndex(NoteContract.NoteEntry.COLUMN_TITLE));
        String body = mCursor.getString(mCursor.getColumnIndex(NoteContract.NoteEntry.COLUMN_BODY));
        long id = mCursor.getLong(mCursor.getColumnIndex(NoteContract.NoteEntry.COLUMN_ID));

        holder.txtNoteTitle.setText(title);
        holder.txtNoteBody.setText(body);
        holder.itemView.setTag(id);

        Log.d(TAG, mCursor.getString(mCursor.getColumnIndex(NoteContract.NoteEntry.COLUMN_TIMESTAMP)));
    }

    @Override
    public int getItemCount() {
        return mCursor.getCount();
    }

    // If database is updated, this method is called to update cursor and
    // notify adapter about it
    public void swapCursor(Cursor newCursor) {
        if (mCursor != null) {
            mCursor.close();
        }

        mCursor = newCursor;
        if (newCursor != null) {
            notifyDataSetChanged();
        }
    }
}
